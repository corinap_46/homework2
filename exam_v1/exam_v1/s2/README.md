# Subiectul 2: Javascript

## Se da fisierul index.html ce contine un input cu id-ul `person-name`;
## Sa se adauge in fisierul `script.js` un callback la evenimentul de `keypress` pentru a permite doar adaugarea literelor.
## (HINT: Se vor folosi valorile din tabelul ASCII si proprietatea `charCode` a obiectului de tip eveniment)